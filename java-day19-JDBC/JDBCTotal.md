# 什么是存储过程？
存储过程（Stored Procedure）是一组预编译的SQL语句，它们被存储在数据库服务器上，可以被多次调用。存储过程通常用于执行一系列数据库操作，例如插入、更新、删除数据，或者执行查询操作。存储过程有以下特点：

- 预编译： 存储过程在第一次创建或修改时进行编译，然后保存在数据库中，因此可以在后续的调用中直接执行，而无需重新编译。

- 封装性： 存储过程允许将一系列SQL语句封装在一个单一的过程中，提供了一种组织和管理数据库逻辑的方法。

- 安全性： 存储过程可以根据数据库用户的权限进行访问控制，提高了数据库的安全性。
# java 中如何调用存储过程？如何注册 OUT 参数，如何取 OUT 参数的值

Java中调用存储过程的一般步骤：

1. 建立数据库连接： 使用数据库连接池或直接创建数据库连接。

2. 创建CallableStatement对象： 使用Connection对象的prepareCall方法创建CallableStatement对象，该对象用于执行存储过程。

3. 设置参数： 如果存储过程接受参数，可以使用setXXX方法设置参数的值，其中XXX是参数类型，如setInt、setString等。

4. 执行存储过程： 调用CallableStatement对象的execute方法执行存储过程。

5. 获取结果： 如果存储过程返回结果，可以使用getXXX方法获取结果，其中XXX是结果类型，如getInt、getString等。

6. 关闭连接和资源： 在完成操作后，关闭CallableStatement、Connection等资源，以释放数据库连接。

在Java中，要注册OUT参数并获取其值，你需要使用JDBC的`CallableStatement`对象提供的方法。以下是如何注册OUT参数和获取其值的步骤：

注册OUT参数： 在创建CallableStatement对象后，使用`registerOutParameter`方法注册OUT参数。这个方法的语法如下：

- `callableStatement.registerOutParameter(parameterIndex, sqlType);`
    - parameterIndex：参数的索引，从1开始。
    - sqlType：OUT参数的SQL类型，例如Types.INTEGER表示整数类型。
执行存储过程： 在注册OUT参数后，执行存储过程，通常使用`execute`方法。

获取OUT参数的值： 在存储过程执行后，可以使用getXXX方法获取OUT参数的值，其中XXX是参数的类型，如getInt、getString等。
